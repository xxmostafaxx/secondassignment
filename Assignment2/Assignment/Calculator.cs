﻿using System;
namespace Assignment2
{
    public class Calculator
    {
        int[] numbers;
        public Calculator(int[] numbers)
        {
            this.numbers = numbers;
        }
        public int Sum()
        {
            int result = 0;
            foreach (int num in numbers) { result += num; }
            return result;
        }
        public int Subtraction()
        {
            /* sum them first then subtract them
             * from a number that you should pass to the function
             */
            int result = numbers[0];
            foreach (int num in numbers) { result -= num; }
            return result + numbers[0];
        }
        public int Division(int divideby)
        {
            /* handle slkfl;gksf;the following scenarios
             * if the numbers array was empty
             * the divideby was zero
            */
            return Sum() / divideby;
        }
        public int Multiplication(int multiplyby)
        {
            return Sum() / multiplyby;
        }
    }
}
